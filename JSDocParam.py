import sublime_plugin

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
from copy import copy
#------------------------------------------------------------------------------#
# Constants:
#------------------------------------------------------------------------------#
PARAM_LINE_BLANK = ['*', '', '', '', '']
PARAM_LINE_EMPTY = ['', '', '', '', '']
RETURN_LINE_BLANK = ['', '', '', '']
#------------------------------------------------------------------------------#
def blanklines_ignore(input1):
    output = []
    line_previous = []
    for line in input1:
        if line == PARAM_LINE_BLANK   and   line == line_previous: # Duplicate blank line.
            continue # Skip this line.
        output.append(line)
        line_previous = line
    return output
#------------------------------------------------------------------------------#
def returnline_format(whitespace_leading, input1):
    """ Return 'input' formatted as a special "return line". """
    fields = copy(RETURN_LINE_BLANK)

    string = input1.strip()
    fields[0] = string[0]

    string = string[1:].lstrip() # Skip the expected asterisk.
    (head, sep, tail) = string.partition('{') #pylint: disable=unused-variable
    fields[1] = head.rstrip()

    string = tail
    (head, sep, tail) = string.partition('}')
    head = head.strip() # Remove all surrounding whitespace. Considered removing *internal* whitespace, but that wasn't requested.
    fields[2] = '{'+head+'}'

    string = tail.lstrip()
    string = string.lstrip('-')
    fields[3] = string.strip()

    return '%s%s %s %s%s%s' % (whitespace_leading, fields[0], fields[1], fields[2], ' ' if fields[3] else '', fields[3])
#------------------------------------------------------------------------------#
def returnline_identified(input1):
    """ Return True if input is a special "return line", else False.  """
    string = input1.lstrip()
    string = string[1:].lstrip() # Skip the expected asterisk.
    (head, sep, tail) = string.partition('{') #pylint: disable=unused-variable
    output = (head.rstrip() in ['@return', '@returns'])
    # if head.rstrip() in ['@return', '@returns']:
    #     output = True
    # else:
    #     output = False
    return output
#------------------------------------------------------------------------------#
def stringlist_format(input1):
    """
    Return a list of strings formatted per the following requirements.

    I need Python code that will accept something like this as an array of strings (excluding the dashed lines):

    ----------------------------------------------------------------------------
     * @param {module:app/model/model~Model} model - The model definition
     * @param {string} mode - The mode being performed (e.g. "add", "edit")
     * @param {string} name - The name of the field (e.g. "type")
     * @param {function} callback - The Node-style callback to invoke with the result
     * @param {?module:javascript~Error} callback.err - The error object
     * @param {string} callback.s - The authorization level
    ----------------------------------------------------------------------------

    and reformat it into this:

    ----------------------------------------------------------------------------
     * @param {module:app/model/model~Model} model        - The model definition
     * @param {string}                       mode         - The mode being performed (e.g. "add", "edit")
     * @param {string}                       name         - The name of the field (e.g. "type")
     * @param {function}                     callback     - The Node-style callback to invoke with the result
     * @param {?module:javascript~Error}     callback.err - The error object
     * @param {string}                       callback.s   - The authorization level
    ----------------------------------------------------------------------------

    The general format of an input line is:

         * @param {xxx} [yyy] - zzz
        ------------------------------
        123456789 123456789 123456789
                 1         2         3
                 0         0         0

    where:

      - Positions 1, 3, 10, 16, 22, 24, 28: Can represent 0 or more whitespace characters
      - "xxx" represents any characters other than a "}"
      - The "[" and "]" are themselves optional
      - "yyy" represents 1 or more non-whitespace characters
      - "zzz" represents 0 or more characters of any kind

    Enhancement 1:

        All lines should match the indentation of the first line. So, whatever
        whitespace appears before the * on the first line should appear on all lines.

        Implementation:

            All leading whitespace from the first string is saved and applied to all lines.

    Enhancement 2:

        If there's nothing but whitespace after the *, then leave the line as-is.
        This allows the user to have blank (except for the leading *) lines between
        their @params, if they wish.

        Implementation:

            All trailing whitespace from asterisk lines is discarded.
            Contrast to Enhancement 1.

    Enhancement 3:

        The general format of the *last* line might be different, specifically this:

             * @xxx { yyy } - zzz
            ------------------------------
            123456789 123456789 123456789
                     1         2         3
                     0         0         0

        where:

          - "xxx" is either "return" or "returns"
          - "yyy" represents 1 or more non-whitespace characters
          - "zzz" represents 0 or more characters of any kind
          - any whitespace (positions 1, 3, 8, 10, 14, 16, 20) represents 0 or more whitespace characters
          - the "-" in position 17 may or may not be present

        The last line should be reformatted to:

             * @returns {yyy} zzz
            ------------------------------
            123456789 123456789 123456789
                     1         2         3
                     0         0         0

        where:

          - column 1 represents the indentation to match the first @param
          - columns 3, 12 and 18 represent a single space

        For example, this:

        ------------------------------------------------------------------------
         *         @return      {string}     -       The formatted result
        ------------------------------------------------------------------------

        would be reformatted to this:

        ------------------------------------------------------------------------
         * @returns {string} The formatted result
        ------------------------------------------------------------------------

    Enhancement 4:

        (1) If the line array contains only a @returns line, make sure it is reformatted properly.

        (2) If there is more than one "blank" line (either blank or just a "*" surrounded
            by whitespace) between the @param and @return sections, reduce it to just one.

        (3) If there are no "blank" lines between the @param and @return sections, insert one.

    """
    if returnline_identified(input1[-1]): # Is the last line a "return line"?
        returnline = input1.pop() # Remove and save that line for special formatting.
    else:
        returnline = ''

    input_exploded = []
    whitespace_leading = ''
    for string in input1:
        if not input_exploded: # First string.
            whitespace_leading = string[:len(string) - len(string.lstrip())]
        input_exploded.append(string_explode(string))

    if returnline   and   len(input1) != 0: # Both param lines and return lines exist.
        input_exploded.append(copy(PARAM_LINE_BLANK)) # Ensure there's a blank line between param and return lines. Duplicates will be ignored later.

    length = string_lengthsmax(input_exploded)

    output = []
    for input1 in blanklines_ignore(input_exploded):
        output.append(string_format(whitespace_leading, input1, length))

    if returnline:
        if len(input1) == 0: # returnline is the *only* line.
            whitespace_leading = ' ' # Just a single leading whitespace character.
        output.append(returnline_format(whitespace_leading, returnline))

    return output
#------------------------------------------------------------------------------#
def string_explode(input1):
    """ Return 'input' separated into 'fields'. """
    fields = copy(PARAM_LINE_EMPTY)

    if input1.strip() == '': # A genuinely blank line.
        fields = copy(PARAM_LINE_BLANK) # Force a blank line.
    else:
        string = input1.strip()
        fields[0] = string[0]

        string = string[1:].lstrip()
        if string != '': # Not a "blank" line. Continue parsing the input.
            (head, sep, tail) = string.partition('{')
            fields[1] = head.rstrip()

            string = tail
            (head, sep, tail) = string.partition('}')
            head = head.strip() # Remove all surrounding whitespace.
            fields[2] = '{'+head+'}'

            string = tail.lstrip()
            field3_firstchar = string[0]
            string = string.lstrip('[')
            (head, sep, tail) = string.partition(']')
            if sep == ']':
                if field3_firstchar == '[':
                    fields[3] = '[%s]' % head.strip() # Keep paired square brackets. Remove all surrounding whitespace.
                else:
                    fields[3] = head.strip() # Discard unpaired square brackets. Remove all surrounding whitespace.
            else:
                (head, sep, tail) = string.partition(' ')
                if sep == ' ':
                    fields[3] = head.strip() # Remove all surrounding whitespace.
                else:
                    (head, sep, tail) = string.partition('-')
                    if sep == '-':
                        fields[3] = head.strip() # Remove all surrounding whitespace.
                    else:
                        fields[3] = string.strip() # Remove all surrounding whitespace.

            string = tail
            (head, sep, tail) = string.partition('-')
            if sep == '-':
                fields[4] = tail.lstrip()
            else:
                fields[4] = string.lstrip()

    return fields
#------------------------------------------------------------------------------#
def string_format(whitespace_leading, input1, length):
    """ Return a string with each field from 'input' left-justified in column widths from 'length'. """
    output = '%s%-*s %-*s %-*s %-*s%s' % (whitespace_leading, length[0], input1[0], length[1], input1[1], length[2], input1[2], length[3], input1[3], ' - '+input1[4] if input1[4] else '')
    return output.rstrip()
#------------------------------------------------------------------------------#
def string_lengthsmax(input1):
    """ Return max length of each field from 'input' in 'lengths'. """
    lengths = [0, 0, 0, 0, 0]
    for input1 in input1:
        for index in range(len(lengths)):
            if len(input1[index]) > lengths[index]:
                lengths[index] = len(input1[index])
    return lengths
#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#



class JsdocParamCommand(sublime_plugin.TextCommand):
    def run(self, edit):

        # fetch the first selection, extend it to full lines, then refetch it
        region = self.view.sel()[0]
        self.view.run_command("expand_selection", {"to": "line"})
        region = self.view.sel()[0]

        # populate an array of strings, one per line in the selection
        linestrings = []
        lines = self.view.lines(region)
        for line in lines:
            linestrings.append(self.view.substr(line))

        # reformat the strings in the array and return in a new array
        formatted_linestrings = stringlist_format(linestrings)

        # print the reformatted strings to the console
        # for ndx, str in enumerate(formatted_linestrings):
        #     print ndx, str

        # erase the old selection
        self.view.erase(edit, region)

        # insert the reformatted lines at the beginning of the region,
        # in reverse order
        for str1 in reversed(formatted_linestrings):
            self.view.insert(edit, region.begin(), str1 + '\n')
