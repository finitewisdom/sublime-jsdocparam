/**
 * @module grunt

 * @description This module provides helper functions for the Grunt (build) process.
 * See [Getting started with Grunt]{@link http://gruntjs.com/getting-started} for more information.
 */

/**
 * Load Grunt plugins and tasks.
 *
 * @see [the Grunt documentation]{@link http://gruntjs.com/getting-started#loading-grunt-plugins-and-tasks}
 * for more information.
 * 
 * @param  {Object} grunt - The Grunt object that contains Grunt-related methods
 * 
 * @return {undefined} Nothing
 */
function loadNpmTasks( grunt ) {

    grunt.loadNpmTasks( "grunt-jsonlint" );
    grunt.loadNpmTasks( "grunt-pylint" );
}

/**
 * Return the configuration object for the "jsonlint" tool.
 *
 * @see [the grunt-jsonlint documentation]{@link https://www.npmjs.com/package/grunt-jsonlint}
 * for more information.
 * 
 * @return {Object} The configuration object
 */
function getJsonlintConfig() {

    return {
        "default": {
            "src": [
                "./messages.json",
                "./Main.sublime-menu"
            ]
        }
    };
}

/**
 * Return the configuration object for the "pylint" tool.
 *
 * @see [the grunt-pylint documentation]{@link https://www.npmjs.com/package/grunt-pylint}
 * for more information.
 * 
 * @return {Object} The configuration object
 */
function getPylintConfig() {

    return {
        "options": {
            "disable": [
                "consider-using-enumerate",
                "I0011",
                "import-error",
                "line-too-long",
                "missing-docstring",
                "too-few-public-methods",
                "too-many-branches"
            ]
        },
        "default": {
            "src": [
                "./*.py"
            ]
        }
    };
}

/**
 * Every Grunt configuration file uses this basic format. All of our Grunt code must be specified inside this function.
 *
 * @see [the Grunt documentation]{@link http://gruntjs.com/getting-started#the-wrapper-function} for more information.
 * 
 * @param {Object} grunt - The Grunt object that contains Grunt-related methods
 * 
 * @return {undefined} Nothing
 */
module.exports = function configure( grunt ) {

    "use strict";

    //  load grunt plugins and tasks.
    loadNpmTasks( grunt );
 
    //  initialize configuration for all tools
    grunt.initConfig( {
        "jsonlint": getJsonlintConfig(),
        "pylint": getPylintConfig()
    } );
 
    //  register the default task
    grunt.registerTask(
        "default",
        [
            "jsonlint"
        ]
    );
 
};
